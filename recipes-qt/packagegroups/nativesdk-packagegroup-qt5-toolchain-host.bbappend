RDEPENDS_${PN} += " \
    nativesdk-icu \
    nativesdk-cmake \
    nativesdk-qttools-tools \
    nativesdk-qtdeclarative-tools \
    nativesdk-qtxmlpatterns-tools \
    nativesdk-perl-module-errno \
    nativesdk-perl-module-english \
    nativesdk-perl-module-tie-file \
    nativesdk-perl-module-file-path \
    nativesdk-perl-module-file-basename \
    nativesdk-perl-module-tie-hash-namedcapture \
"


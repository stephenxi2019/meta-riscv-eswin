RDEPENDS_${PN}_remove += " \
    qtwebkit-dev \
    qtwebkit-mkspecs \
    qtwebkit-qmlplugins \
    \
    qttranslations-qtbase \
    qttranslations-qthelp \
    \
    qtconnectivity-dev \
    qtconnectivity-mkspecs \
    qtconnectivity-qmlplugins \
    qttranslations-qtconnectivity \
    \
    qttranslations-qmlviewer \
    qttranslations-qtdeclarative \
    \
    qtenginio-dev \
    qtenginio-mkspecs \
    qtenginio-qmlplugins \
    \
    qtlocation-dev \
    qtlocation-mkspecs \
    qtlocation-plugins \
    qtlocation-qmlplugins \
    qttranslations-qtlocation \
    \
    qttranslations-qtmultimedia \
    \
    qtscript-dev \
    qtscript-mkspecs \
    qttranslations-qtscript \
    \
    qtsystems-dev \
    qtsystems-mkspecs \
    qtsystems-qmlplugins \
    \
    qtwebsockets-dev \
    qtwebsockets-mkspecs \
    qtwebsockets-qmlplugins \
    qttranslations-qtwebsockets \
    \
    qtwebchannel-dev \
    qtwebchannel-mkspecs \
    qtwebchannel-qmlplugins \
    \
    qttranslations-qtxmlpatterns \
"

RDEPENDS_${PN} += " \
    qtquickcontrols2-qmlplugins \
    \
    qtvirtualkeyboard-dev \
    qtvirtualkeyboard-mkspecs \
    qtvirtualkeyboard-plugins \
    qtvirtualkeyboard-qmlplugins \
    \
    qtcharts-dev \
    qtcharts-mkspecs \
    qtcharts-plugins \
    qtcharts-qmlplugins \
    \
    qtdatavis3d-dev \
    qtdatavis3d-mkspecs \
    qtdatavis3d-plugins \
    qtdatavis3d-qmlplugins \
    \
    qtgamepad-dev \
    qtgamepad-mkspecs \
    qtgamepad-plugins \
    qtgamepad-qmlplugins \
    \
    qtnetworkauth-dev \
    qtnetworkauth-mkspecs \
    qtnetworkauth-plugins \
    qtnetworkauth-qmlplugins \
    \
    qtremoteobjects-dev \
    qtremoteobjects-mkspecs \
    qtremoteobjects-plugins \
    qtremoteobjects-qmlplugins \
    \
    qtscxml-dev \
    qtscxml-mkspecs \
    qtscxml-plugins \
    qtscxml-qmlplugins \
    \
    qtsensors-dev \
    qtsensors-mkspecs \
    qtsensors-plugins \
    qtsensors-qmlplugins \
    \
    qtwebchannel-dev \
    qtwebchannel-mkspecs \
    qtwebchannel-plugins \
    qtwebchannel-qmlplugins \
    \
    qtwebsockets-dev \
    qtwebsockets-mkspecs \
    qtwebsockets-plugins \
    qtwebsockets-qmlplugins \
    \
    qtwebglplugin-dev \
    qtwebglplugin-plugins \
    qtwebglplugin-qmlplugins \
"


FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

QT_CONFIG_FLAGS += " \
    -c++std c++1z \
"

PACKAGECONFIG_GL = "gles2 eglfs"
PACKAGECONFIG_DEFAULT_remove = "tests"
PACKAGECONFIG_append = " accessibility linuxfb freetype libpng jpeg sql-sqlite openssl dbus examples"

PACKAGECONFIG_remove = " \
    gl glib xcb udev tslib icu \
"

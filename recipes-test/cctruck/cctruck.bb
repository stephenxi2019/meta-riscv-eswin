SUMMARY = "Example for Project To Dashboard"
SECTION = "autoio"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/${LICENSE};md5=0835ade698e0bcf8506ecda2f7b4f302"

require recipes-qt/qt5/qt5.inc

# prevent already-stripped QA Issue
INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP_${PN} += " \
    already-stripped \
    installed-vs-shipped arch \
"

PR = "r0"
PV = "1.0"
SRCBRANCH = "master"
SRCREV = "d178d68fe8a61c91aa6353c328a812b3af3d9f17"
SRC_URI += " \
    git://git@gitee.com/opensource-projects/cctruck.git;branch=${SRCBRANCH};protocol=http \
"

S = "${WORKDIR}/git"

DEPENDS += " \
    qtdeclarative-native \
    qtdeclarative \
    qtquickcontrols \
    qtquickcontrols2 \
    qtgraphicaleffects \
"

RDEPENDS_${PN} = " \
    qtdeclarative-qmlplugins \
    qtquickcontrols-qmlplugins \
    qtquickcontrols2-qmlplugins \
    qtgraphicaleffects-qmlplugins \
"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${B}/bin/cctruck ${D}${bindir}/cctruck

    install -d ${D}${libdir}
    install -d ${D}${libdir}/Fonts
    cp ${S}/Fonts/* ${D}${libdir}/Fonts/
}

FILES_${PN} += " \
    ${bindir}/bin \
    ${libdir}/Fonts \
"

do_install_append() {
    echo "export QT_QPA_PLATFORM=wayland" >> ${D}${ROOT_HOME}/.profile
    echo "export XDG_RUNTIME_DIR=/run/user/0" >> ${D}${ROOT_HOME}/.profile
    echo "export WAYLAND_DISPLAY=wayland-0" >> ${D}${ROOT_HOME}/.profile
    echo "export QT_QPA_FONTDIR=/usr/lib/Fonts" >> ${D}${ROOT_HOME}/.profile
}

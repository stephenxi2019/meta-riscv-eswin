DESCRIPTION = "RISC-V Core IP Base Linux image"

require recipes-core/images/core-image-minimal.bb

inherit extrausers
EXTRA_USERS_PARAMS = "usermod -P sifive root;"

IMAGE_FEATURES += "\
    splash \
    package-management \
    ssh-server-openssh \
    nfs-client nfs-server \
"

# base libs and tools
IMAGE_INSTALL += " \
    kernel-modules \
    bc kmod memtester \
    iperf3 iproute2 lrzsz sysstat \
    lmbench sysbench tinymembench tree \
    curl wget stress-ng \
    procps bind-utils pciutils sysfsutils usbutils \
"

# wayland init script: weston-start &
IMAGE_INSTALL += " \
    wayland \
    wayland-protocols \
    \
    weston-init \
    weston-examples \
"

# qt libs and tools
IMAGE_INSTALL += " \
    qtsvg \
    qtconnectivity \
    qtserialport \
    qtbase-plugins \
    qtbase-examples \
    qtserialbus-tools \
    qtserialbus-plugins \
    qt3d-qmlplugins \
    qt3d-examples \
    qtquick3d-qmlplugins \
    qtdeclarative-qmlplugins \
    qtquickcontrols-qmlplugins \
    qtquickcontrols2-qmlplugins \
    qtgraphicaleffects-qmlplugins \
    qtgamepad-qmlplugins \
    qtlocation \
    qtpurchasing \
    qtmultimedia-qmlplugins \
    qtmultimedia-examples \
    qtnetworkauth-dev \
    qtvirtualkeyboard-qmlplugins \
    qtvirtualkeyboard-examples \
    qtremoteobjects-qmlplugins \
    qtsensors-qmlplugins \
    qtscxml-qmlplugins \
    qtwayland-qmlplugins qtwayland-tools \
    qtcharts-qmlplugins \
    qtdatavis3d-qmlplugins \
    qtwebchannel-qmlplugins \
    qtwebsockets-qmlplugins \
    qtwebglplugin-qmlplugins \
"

IMAGE_INSTALL += " \
    cctruck \
    cctruck-tools \
"
